package myAuthenticationServer;

import myAuthenticationServer.SecDatagramSocket;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.DatagramPacket;
import java.net.MulticastSocket;
import java.nio.ByteBuffer;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.security.Security;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.Arrays;
import java.util.Properties;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.Mac;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

import utils.UtilsBase;
import java.net.SocketAddress;

public class SecDatagramSocket extends MulticastSocket {

	private static final String CIPHERSUITE = "ciphersuite";
	private static final String KEYSTOREFILE = "./src/mykeystore.jceks";
	private static final String JCEKS = "jceks";
	private static final String AES_KEY = "aeskey";
	private static final String CIPHERSUITE_CONF_PROXY = "./src/hjUDPproxy/conf/ciphersuite.conf";
	private static final String CIPHERSUITE_CONF_SERVER = "./src/hjStreamServer/conf/ciphersuite.conf";
	private static final String PASSWORD = "keystorepassword";
	private static final String HMAC_KM = "hmackeySHA256KM";
	private static final String HMAC_KA = "hmackeySHA256KA";
	private static final String HMAC_SHA256 = "HMacSHA256";
	private static final String BC = "BC";
	private static final String ECB = "ECB";
	private static final String GCM = "GCM";
	private static final String BAR = "/";
	private static final String AES_KEYSIZE = "aeskeySize";
	private static final int MIL_E_UM = 1001;
	private static final int TWELVE = 12;
	private static final int SIXTEEN = 16;
	private static final byte[] FILLER_ZEROS = new byte[Short.MAX_VALUE * 2 + 1];

	// constants for version, separator and payload type (in this version is only
	// type 1
	private static byte VERSION = (byte) 00010001, SEPARATOR = 0x00, PAYLOAD_TYPE_1 = 0x01;
	// payload type (for phase 1 is only type 1)
	private byte payload_type;
	// payload size (2 bytes or short integer)
	private short payload_size;
	private String ciphersuite, keystoretype, keystorefile;

	private String keystorepassword;
	private SecureRandom secRand;

	private int aesKeySize;

	private String aeskey;
	private String hmackeySHA256KM;
	private String hmackeySHA256KA;

	private Key cipherKey;
	private Key kmKey;
	private Key kaKey;
	private long nonce;

	public SecDatagramSocket() throws IOException, UnrecoverableKeyException, KeyStoreException,
			NoSuchAlgorithmException, CertificateException {
		super();
		payload_type = PAYLOAD_TYPE_1;
		getCipherProperties(CIPHERSUITE_CONF_SERVER);
		secRand = new SecureRandom();
		initProcess();
	}

	public SecDatagramSocket(SocketAddress inSocketAddress) throws KeyStoreException, NoSuchAlgorithmException,
			CertificateException, FileNotFoundException, IOException, UnrecoverableKeyException {
		super(inSocketAddress);
		payload_type = PAYLOAD_TYPE_1;

		getCipherProperties(CIPHERSUITE_CONF_PROXY);
		secRand = new SecureRandom();
		initProcess();
	}

	public SecDatagramSocket(int port) throws KeyStoreException, NoSuchAlgorithmException, CertificateException,
			FileNotFoundException, IOException, UnrecoverableKeyException {

		super(port);
		payload_type = PAYLOAD_TYPE_1;

		getCipherProperties(CIPHERSUITE_CONF_PROXY);
		secRand = new SecureRandom();
		initProcess();
	}

	private void initProcess() throws KeyStoreException, NoSuchAlgorithmException, CertificateException,
			FileNotFoundException, IOException, UnrecoverableKeyException {
		File file = new File(KEYSTOREFILE);

		final KeyStore keyStore = KeyStore.getInstance(JCEKS);
		if (file.exists()) {
			// .keystore file already exists => load it
			keyStore.load(new FileInputStream(file), keystorepassword.toCharArray());
		} else {
			// .keystore file not created yet => create it
			keyStore.load(null, null);
			keyStore.store(new FileOutputStream(KEYSTOREFILE), keystorepassword.toCharArray());
		}

		cipherKey = keyStore.getKey(aeskey, keystorepassword.toCharArray());
		kmKey = keyStore.getKey(hmackeySHA256KM, keystorepassword.toCharArray());
		kaKey = keyStore.getKey(hmackeySHA256KA, keystorepassword.toCharArray());
	}

	/**
	 * Creates a header in order to be able to send a packet.
	 * 
	 * @param payload_type
	 * @param payload_size
	 * @return
	 */
	private byte[] createHeader(byte payload_type, short payload_size) {
		ByteBuffer header = ByteBuffer.allocate(6);
		// make header for phase 1 (only type 1)
		header.put(VERSION).put(SEPARATOR).put(payload_type).put(SEPARATOR).putShort(payload_size);

		return header.array();
	}

	private byte[] createPayload(byte[] message, int ivSize, String[] cipher_type)
			throws IOException, IllegalBlockSizeException, BadPaddingException, NoSuchAlgorithmException,
			NoSuchProviderException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException {

		Cipher cipher = Cipher.getInstance(ciphersuite, BC);

		long id = (long) Math.random() * MIL_E_UM;
		nonce = id * System.currentTimeMillis();
		byte[] iv = null;
		if (ivSize != 0) {
			// uses iv

			iv = new byte[ivSize];
			secRand.nextBytes(iv);
		}

		// id
		ByteBuffer idBytes = ByteBuffer.allocate(Long.BYTES);
		idBytes.put(UtilsBase.longToBytes(id));

		// nonce
		ByteBuffer nonceBytes = ByteBuffer.allocate(Long.BYTES);
		nonceBytes.put(UtilsBase.longToBytes(nonce));

		// Mp = [id || nonce || M]
		ByteBuffer Mp = ByteBuffer.allocate(nonceBytes.capacity() + idBytes.capacity() + message.length);
		Mp.put(idBytes).put(nonceBytes).put(message);

		// MacKM(Mp)
		Mac hmacKm = Mac.getInstance(HMAC_SHA256);
		hmacKm.init(kmKey);
		byte[] MacKmMp = hmacKm.doFinal(Mp.array());

		// Mp || MacKM(Mp)
		ByteBuffer msgToEncrypt = ByteBuffer.allocate(Mp.capacity() + MacKmMp.length);
		msgToEncrypt.put(Mp).put(MacKmMp);

		// initialize cipher
		if (ivSize != 0) {
			cipher.init(Cipher.ENCRYPT_MODE, cipherKey, new IvParameterSpec(iv));
		} else {
			cipher.init(Cipher.ENCRYPT_MODE, cipherKey);
		}

		byte[] c = null;
		// C = E(Ks, [Mp || MacKM(Mp)])
		c = cipher.doFinal(msgToEncrypt.array());

		Mac hmacKa = Mac.getInstance(HMAC_SHA256);
		hmacKa.init(kaKey);
		byte[] MacKaC = hmacKa.doFinal(c);

		ByteBuffer ivSizeBuf = ByteBuffer.allocate(Integer.BYTES);
		ivSizeBuf.putInt(ivSize);

		byte[] payload = new byte[c.length + MacKaC.length + ivSize + Integer.BYTES];
		System.arraycopy(ivSizeBuf.array(), 0, payload, 0, Integer.BYTES);

		if (ivSize != 0)
			System.arraycopy(iv, 0, payload, ivSize, iv.length);

		System.arraycopy(c, 0, payload, Integer.BYTES + ivSize, c.length);
		System.arraycopy(MacKaC, 0, payload, Integer.BYTES + c.length + ivSize, MacKaC.length);

		return payload;
	}

	@Override
	public void send(DatagramPacket packet) throws IOException {
		byte[] header = createHeader(payload_type, payload_size);
		byte[] encryptedMessage = null;

		try {
			byte[] tmp = new byte[packet.getLength()];
			System.arraycopy(packet.getData(), 0, tmp, 0, packet.getLength());
			encryptedMessage = encryptMessage(tmp);
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidAlgorithmParameterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchProviderException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		byte[] packetToSend = new byte[header.length + encryptedMessage.length];

		// copy information to packetToSend, in order to have header plus payload
		System.arraycopy(header, 0, packetToSend, 0, header.length);
		System.arraycopy(encryptedMessage, 0, packetToSend, header.length, encryptedMessage.length);

		super.send(new DatagramPacket(packetToSend, packetToSend.length, packet.getAddress(), packet.getPort()));
	}

	@Override
	public synchronized void receive(DatagramPacket packet) throws IOException {
		super.receive(packet);
		byte[] dataBuffer = new byte[packet.getLength()];
		System.arraycopy(packet.getData(), 0, dataBuffer, 0, packet.getLength());
		System.arraycopy(FILLER_ZEROS, 0, packet.getData(), packet.getLength(),
				packet.getData().length - packet.getLength());
		packet.setLength(packet.getData().length);
		try {
			byte[] decryptedMsg = decryptMessage(dataBuffer);
			Arrays.fill(packet.getData(), (byte) 0x00);

			System.arraycopy(decryptedMsg, 0, packet.getData(), 0, decryptedMsg.length);
		} catch (Exception e) {
			throw new IOException(e);
		}

	}

	/**
	 * 
	 * Encrypts the data from the packet and returns the encrypted message ready to
	 * be appended to the final packet to be sent.
	 * 
	 * @param message - message to be encrypted
	 * @return encryptedMessage
	 * @throws IOException
	 * @throws InvalidKeyException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 * @throws InvalidAlgorithmParameterException
	 * @throws NoSuchPaddingException
	 * @throws NoSuchProviderException
	 * @throws NoSuchAlgorithmException
	 */
	private byte[] encryptMessage(byte[] message) throws IOException, InvalidKeyException, IllegalBlockSizeException,
			BadPaddingException, InvalidAlgorithmParameterException, NoSuchAlgorithmException, NoSuchProviderException,
			NoSuchPaddingException {
		byte[] payload = null;

		String[] cipher_type = ciphersuite.split(BAR);
		switch (cipher_type[1]) {
		case ECB:
			payload = createPayload(message, 0, cipher_type);
			break;
		case GCM:
			payload = createPayload(message, TWELVE, cipher_type);
			break;
		default:
			payload = createPayload(message, SIXTEEN, cipher_type);
			break;
		}
		return payload;
	}

	/**
	 * Decrypts an encrypted message and returns the plaintext
	 * 
	 * @param key        - secret key
	 * @param cipherText - encrypted message
	 * @param ivSpec     - Initialization vector
	 * @return plaintext (decrypted message)
	 * @throws Exception
	 */
	private byte[] decryptMessage(byte[] cipherText) throws Exception {
		Cipher cipher = Cipher.getInstance(ciphersuite, BC);

		// MacKa(C)
		Mac hmacKa = Mac.getInstance(HMAC_SHA256);
		hmacKa.init(kmKey);

		byte[] ivSize = new byte[Integer.BYTES];
		byte[] iv = null;

		System.arraycopy(cipherText, 6, ivSize, 0, Integer.BYTES);

		ByteBuffer ivValue = ByteBuffer.wrap(ivSize);

		if (ivValue.getInt(0) != 0) {
			// iv exists
			iv = new byte[ivValue.getInt(0)];
			System.arraycopy(cipherText, Integer.BYTES + 6, iv, 0, ivValue.getInt(0));
		}

		int macKaLength = hmacKa.getMacLength();
		byte[] MacKaC = new byte[macKaLength];
		byte[] encryptedMsg = new byte[cipherText.length - macKaLength - Integer.BYTES - ivValue.getInt(0) - 6];
		byte[] decryptedMsg = null;
		// copy MacKa(C)
		System.arraycopy(cipherText, cipherText.length - macKaLength, MacKaC, 0, macKaLength);
		// copy encrypted part of message
		System.arraycopy(cipherText, 6 + Integer.BYTES + ivValue.getInt(0), encryptedMsg, 0,
				cipherText.length - macKaLength - Integer.BYTES - ivValue.getInt(0) - 6);

		if (ivValue.getInt(0) != 0) {
			// message decryption
			cipher.init(Cipher.DECRYPT_MODE, cipherKey, new IvParameterSpec(iv));

			decryptedMsg = cipher.doFinal(encryptedMsg);
		} else {
			cipher.init(Cipher.DECRYPT_MODE, cipherKey);
			decryptedMsg = cipher.doFinal(encryptedMsg);
		}

		// encrypt Mp with MackKm
		Mac hmacKm = Mac.getInstance(HMAC_SHA256);
		hmacKm.init(kmKey);
		int macKmLength = hmacKm.getMacLength();

		byte[] Mp = new byte[decryptedMsg.length - macKmLength];
		byte[] MacKmMp = new byte[macKmLength];

		System.arraycopy(decryptedMsg, 0, Mp, 0, Mp.length);
		System.arraycopy(decryptedMsg, Mp.length, MacKmMp, 0, MacKmMp.length);

		byte[] encryptedMp = hmacKm.doFinal(Mp);
		if (MessageDigest.isEqual(encryptedMp, MacKmMp)) {
			// corrupted
			throw new SecurityException("Integrity check failed.");
		}

		// verify nonce
		byte[] id = new byte[Long.BYTES];
		byte[] nonce = new byte[Long.BYTES];
		byte[] M = new byte[Mp.length - SIXTEEN];

		System.arraycopy(Mp, 0, id, 0, Long.BYTES);
		// copy Mp nonce
		System.arraycopy(Mp, Long.BYTES, nonce, 0, Long.BYTES);
		ByteBuffer idBuf = ByteBuffer.allocate(Long.BYTES);
		ByteBuffer nonceBuf = ByteBuffer.allocate(Long.BYTES);
		nonceBuf.put(nonce);

		long confNonce = idBuf.getLong(0) * System.currentTimeMillis();
		if (confNonce < nonceBuf.getLong(0)) {
			// corrupted
			throw new SecurityException("Nonce is invalid");
		}
		// get M
		System.arraycopy(Mp, SIXTEEN, M, 0, Mp.length - SIXTEEN);
		return M;
	}

	/**
	 * Fetch properties from file ciphersuite.conf
	 * 
	 * @param filename
	 */
	private void getCipherProperties(String filename) {

		try {
			InputStream inputStream;
			inputStream = new FileInputStream(new File(filename));

			Properties properties = new Properties();

			properties.load(inputStream);

			ciphersuite = properties.getProperty(CIPHERSUITE);
			keystorefile = properties.getProperty(KEYSTOREFILE);
			keystoretype = properties.getProperty(JCEKS);
			keystorepassword = properties.getProperty(PASSWORD);
			aeskey = properties.getProperty(AES_KEY);
			aesKeySize = Integer.parseInt(properties.getProperty(AES_KEYSIZE));
			hmackeySHA256KM = properties.getProperty(HMAC_KM);
			hmackeySHA256KA = properties.getProperty(HMAC_KA);

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
